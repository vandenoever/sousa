import QtQuick 2.0
import QtQuick.Controls 1.0
import QtQuick.Dialogs 1.1

ApplicationWindow {
    visible: true

    ListView {
        anchors.fill: parent
        model: dirModel
        delegate: Text {
            text: file_name
            font.italic: is_dir
            MouseArea {
                anchors.fill: parent
                cursorShape: is_dir ? Qt.PointingHandCursor : Qt.ArrowCursor
                onClicked: {
                    if (is_dir) {
                        dirLister.change_directory(file_name);
                    }
                }
            }
        }
    }

    MessageDialog {
        id: messageDialog
        onAccepted: {
            messageDialog.visible = false;
        }
    }

    Component.onCompleted: {
        if (dirLister.error && dirLister.error.connect) {
            dirLister.error.connect(function(error) {
                 messageDialog.title = "Error";
                 messageDialog.text = error;
                 messageDialog.visible = true;
            });
        }
    }
}
